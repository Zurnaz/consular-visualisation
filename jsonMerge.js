const csv = require('csv-parser')
const fs = require('fs')
const jsonfile = require('jsonfile')
const outputFile = 'consular.json'
const inputFIle = 'consular.csv'

let jsonLookup = {}

fs.createReadStream(inputFIle)
  .pipe(csv())
  .on('data', function (data) {
    if (!jsonLookup[data.cityname]) {
      // "cityname","casetype","reportdate","lower","upper"
      jsonLookup[data.cityname] = {}
    }
    if (!jsonLookup[data.cityname][data.casetype]) {
      jsonLookup[data.cityname][data.casetype] = {}
    }
    if (!jsonLookup[data.cityname][data.casetype][data.reportdate]) {
      jsonLookup[data.cityname][data.casetype][data.reportdate] = {lower: data.lower, upper: data.upper}
    } else {
      throw Error(`Report already exists: ${data.cityname} | ${data.casetype} | ${data.reportdate}`)
    }
  })
  .on('end', function () {
    jsonfile.writeFile(outputFile, jsonLookup, function (err) {
      console.error(err)
    })
    console.log('# Finished: Writing data to file')
  })
