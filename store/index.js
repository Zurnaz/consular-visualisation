import Vuex from 'vuex'
const cases = require('~/static/cases.json')
const cities = require('~/static/cities.json')
const dates = require('~/static/dates.json')
const caseTypes = require('~/static/caseTypes.json')
const caseTypeGroups = require('~/static/caseTypeGroups.json')
const citiesCountries = require('~/static/citiesCountries.json')

const createStore = () => {
  return new Vuex.Store({
    state: {
      cities,
      dates,
      caseTypes,
      cases,
      caseTypeGroups,
      citiesCountries
    },
    mutations: {
      increment (state) {
        state.counter++
      }
    }
  })
}

export default createStore
